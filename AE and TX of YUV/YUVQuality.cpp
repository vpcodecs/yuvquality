#include "stdafx.h"
#include "YUVQuality.h"



int YUVQuality::beta[7] = { 0, 0, 0, 0, 0, 0, 0 };

void YUVQuality::initializeLUT()
{
	for (int i = 0; i < MAX_ITERATIONS; i++)
	{
		LUT[i] = pow(pow((-i), e), e);
	}
}

void YUVQuality::expand_SGL(float x, int* exponent, int* signum, unsigned int* mantissa)
{
	unsigned int tmp;
	float* pIEEE_754_raw;

	pIEEE_754_raw = &x;
	if (((int&)pIEEE_754_raw & 0x80000000) == 0)
		*signum = 0;
	else
		*signum = 1;

	tmp = (int&)pIEEE_754_raw & 0x7FFFFFFF;
	*exponent = (tmp >> 23) - 127;
	*mantissa = 0x80000000 | (tmp << 8);
}

float YUVQuality::e_exp(float x)
{
	int exponent, signum, offset, iterations, index;
	unsigned int mantissa, test;
	float temp, result;

	expand_SGL(x, &exponent, &signum, &mantissa);			//extract data

	//special cases
	if (exponent <  -MAX_ITERATIONS)						//values below precision
	{
		result = 1;											//case e^0 = 1
	}
	else
	{
		if (exponent == -MAX_ITERATIONS)					//no mult are needed since the last item is chosen only
		{
			if (signum == 0)
				result = LUT[MAX_ITERATIONS];
			else
				result = 1 / LUT[MAX_ITERATIONS];
		}
		else
		{
			if (exponent > 7)								//out of range
			{
				if (signum == 0)
					result = 1E99;							//infinity
				else
					result = 0;								//0
			}
			else
			{
				iterations = MAX_ITERATIONS;				//initialize iterations
				//normal cases
				if (exponent < 0)
				{
					offset = abs(exponent);					//get LUT offset
					iterations += exponent;					//less itearations are required
					index = offset;							//start LUT at offset
					temp = LUT[index];
				}
				//exponent >= 0
				else
				{
					index = 0;								//initialize index
					temp = 1;								//initialize temp for products
				}

				for (int i = 0; i < iterations; i++)
				{
					index++;
					mantissa = mantissa << 1;
					test = mantissa & 0x80000000;			//get most significant bit
					if (test != 0)							//check msb
						temp *= LUT[index];
				}
				//do the final mult if exponent is positive
				if (exponent >= 0)
					result = 2 * temp;
				else
					result = temp;

				//binary exponent
				if (signum == 1)
					result = 1 / result;					//negative value, so inverse

				if (exponent > 0)
					for (int i = 1; i < exponent; i++)		//compute squares
						result *= result;
			}
		}

	}
	return result;
}

float YUVQuality::ED(const char* p, int i, int j, int w, int h)
{
	if (i<1 || i>h - 1 || j>w)
		return 0;

	return abs((int)p[(i - 1)*w + j] - (int)p[(i - 1)*w + j + 1] + 2 * ((int)p[i*w + j] - (int)p[i*w + j + 1]) + (int)p[(i + 1)*w + j] - (int)p[(i + 1)*w + j + 1]) / 4;
}

void YUVQuality::CalculateTX(char* pYBuf, int nWidth, int nHeight, bool* EP)
{
	clock_t start;
	double duration;
	start = clock();
	printf("Calculating TX.\n");
	vector<bool> tempVectorEP;
	//tempVectorEP.resize(nWidth * nHeight);
	for (int i = 0; i < (nWidth / 16) * (nHeight / 16); i++)
	{
		TX[i] = 0.0;
		EDArray[i] = 150.0;
		ST[i] = 0;
		meanArray[i] = 0.0;
		sum[i] = 0.0;
		EP[i] = true;   //This array comes as input from external source, I just set it as all true. 
		tempVectorEP.push_back(true);
	}

	matrixEP.push_back(tempVectorEP);
	//tempVectorEP.clear();
	//tempVectorEP.shrink_to_fit();

	for (int i = 0; i < (nWidth * nHeight); i++)
	{
		structure[i] = 0;
		texture[i] = 0;
	}
	

	printf("Bilateral filter.\n");
	for (int ii = 4; ii <= nHeight - 3; ii++)
	{
		for (int jj = 4; jj <= nWidth - 3; jj++)
		{
			int k = 0;
			printf("Current position that is being processed %d, %d      \r", ii, jj);
			for (int iprim = ii - 3; iprim <= ii + 3; iprim++)
			{
				for (int jprim = jj - 3; jprim <= jj + 3; jprim++)
				{
					structure[ii*nWidth + jj] += pYBuf[iprim * nWidth + jprim] * exp(-abs(pYBuf[ii*nWidth + jj] - pYBuf[iprim*nWidth + jprim]) - sqrt(pow(ii - iprim, 2) + pow(jj - jprim, 2)));
					k += exp(-abs(pYBuf[ii*nWidth + jj] - pYBuf[iprim*nWidth + jprim]) - sqrt(pow(ii - iprim, 2) + pow(jj - jprim, 2)));
				}
			}
			structure[ii*nWidth + jj] /= k;
		}
	}

	printf("\nImage decomposition.\n");
	for (int i = 0; i <= nWidth * nHeight; i++)
		texture[i] = (float)pYBuf[i] - structure[i];

	printf("Downsampling.\n");
	for (int ii = 0; ii < nHeight; ii++)
	{
		for (int jj = 0; jj < nWidth; jj++)
		{
			int x = ii*nWidth + jj;
			ST[((x / ((16 * 16) * (nWidth / 16))) * (nWidth / 16)) + ((x / 16) % (nWidth / 16))] += structure[x];
		}
	}
	//We are calculating the mean
	for (int i = 0; i < (nWidth / 16) * (nHeight / 16); i++)
		ST[i] /= 256;

	printf("Sobel filter.\n");
	for (int ii = 2; ii <= (nHeight / 16) - 1; ii++)
	{
		for (int jj = 2; jj <= (nWidth / 16) - 1; jj++)
		{
			if (EP[ii*(nWidth / 16) + jj] == true)
				EDArray[ii*(nWidth / 16) + jj] = abs(ST[(ii - 1)*(nWidth / 16) + jj + 1] + 2 * ST[(ii + 1)*(nWidth / 16) + jj + 1]
				- ST[(ii - 1)*(nWidth / 16) + jj - 1] - 2 * ST[ii * (nWidth / 16) + jj - 1] - ST[(ii + 1)*(nWidth / 16) + jj - 1])
				+ abs(ST[(ii - 1)*(nWidth / 16) + jj - 1] + 2 * ST[(ii + 1)*(nWidth / 16) + jj] + ST[(ii + 1)*(nWidth / 16) + jj + 1]
				- ST[(ii - 1)*(nWidth / 16) + jj - 1] - 2 * ST[(ii - 1)*(nWidth / 16) + jj] - ST[(ii - 1)*(nWidth / 16) + jj + 1]);
		}
	}

	printf("Calculating variance.\n");
	for (int ii = 0; ii < nHeight; ii++)
	{
		for (int jj = 0; jj < nWidth; jj++)
		{

			int x = ii*nWidth + jj;
			meanArray[((x / ((16 * 16) * (nWidth / 16))) * (nWidth / 16)) + ((x / 16) % (nWidth / 16))] += texture[x];
		}
	}

	for (int i = 0; i < (nWidth / 16) * (nHeight / 16); i++)
		meanArray[i] /= 256;



	for (int ii = 0; ii < nHeight; ii++)
	{
		for (int jj = 0; jj < nWidth; jj++)
		{
			printf("Current position that is being processed %d, %d      \r", ii, jj);
			int x = ii*nWidth + jj;
			int y = ((x / ((16 * 16) * (nWidth / 16))) * (nWidth / 16)) + ((x / 16) % (nWidth / 16));
			if (EDArray[y] < 150)
			{
				sum[y] += texture[x] - meanArray[y];
			}
		}
	}

	printf("\n");
	//This is the variance
	vector<float> tempVectorTX;
	tempVectorTX.resize(nWidth * nHeight);
	//tempVectorTX.reserve((nHeight / 16)* (nWidth / 16));
	for (int i = 0; i < (nWidth / 16) * (nHeight / 16); i++)
	{
		TX[i] = sum[i] / 255;
		tempVectorTX.push_back(TX[i]);
		//printf("TX[%d] = %f\n", i, TX[i]);
	}
	matrixTX.push_back(tempVectorTX);
	tempVectorTX.clear();
	tempVectorTX.shrink_to_fit();

	duration = (clock() - start) / (double)CLOCKS_PER_SEC;
	printf("                                              TIME = %f\n", duration);

}

void YUVQuality::CalculateAE(char* pYBuf, int nWidth, int nHeight)
{
	clock_t start;
	start = clock();
	double duration;
	printf("Continuous edge grouping.\n");
	for (int ii = 2; ii <= nHeight - 1; ii++)
	{
		for (int jj = 1; jj <= nWidth - 1; jj++)
		{
			printf("Current position that is being processed %d, %d      \r", ii, jj);
			if (ED(pYBuf, ii, jj, nWidth, nHeight) >= 12)
			{
				int u = 0, l = 0;
				while (ED(pYBuf, ii + u, jj, nWidth, nHeight) >= 12)
					u++;
				while (ED(pYBuf, ii - l, jj, nWidth, nHeight) >= 12)
					l++;

				if (u + l >= 5)
				{
					CE[ii*nWidth + jj] = ED(pYBuf, ii, jj, nWidth, nHeight);
					if (CES.size() == 0)
						CES.push_back(t3(ii + u, ii - l, jj));
					else
					{
						bool insert = true;
						for each (t3 v in CES)
						{
							if ((ii + u == get<0>(v)) && (ii - l == get<1>(v)) || (jj == get<2>(v)))
							{
								insert = false;
								break;
							}
						}
						if (insert)
							CES.push_back(t3(ii + u, ii - l, jj));
					}
				}
			}
		}
	}

	printf("\nSalient edge filtering.\n");
	for (std::vector<t3>::iterator it = CES.begin(); it != CES.end();)
	{
		t3 v = *it;
		//for each column j + j' - 4
		int jprim = 1;
		while (jprim <= 7)
		{
			if (jprim == 3)
			{
				beta[jprim] = 0;
				jprim++;
				break;
			}
			int s = 0, S = 0;
			//for each neighbour p(i', j+j'-4)
			for (int iprim = get<1>(v); iprim <= get<0>(v); iprim++)
			{
				if (ED(pYBuf, iprim, get<2>(v) +jprim - 4, nWidth, nHeight) >= 5)
				{
					S += ED(pYBuf, iprim, get<2>(v) +jprim - 4, nWidth, nHeight);
					s++;
				}
			}
			if (s == 0)
				beta[jprim] = 0;
			else
				beta[jprim] = S / s;
			jprim++;
		}


		b = beta[0];
		//Calculating the max of beta array.
		for (int kkkk = 0; kkkk < 7; kkkk++)
		{
			if (beta[kkkk] > b)
				b = beta[kkkk];
		}
		int S = 0;
		for (int ii = get<1>(v); ii <= get<0>(v); ii++)
			S += (int)pYBuf[ii*nWidth + get<2>(v)];

		S /= get<0>(v) -get<1>(v) +1;
		if (S < b)
		{
			it = CES.erase(it);
		}
		else
		{
			it++;
		}
	}

	printf("Excluding isolated edges.\n");
	for (std::vector<t3>::iterator it = CES.begin(); it != CES.end();)
	{
		t3 v = *it;
		if ((get<0>(v) -get<1>(v) +1) > 16)
		{
			int Vplus = 0, Vminus = 0;
			//for pixel p(i,j-16) satisfying
			for (int i = get<1>(v); i <= get<0>(v); i++)
			{
				for each(t3 v2 in CES)
				{
					if ((get<1>(v2) <= i <= get<0>(v2)) && get<2>(v2) == get<2>(v) -16)
						Vminus++;
				}
			}
			//for pixel p(i,j+16) satisfying
			for (int i = get<1>(v); i <= get<0>(v); i++)
			{
				for each(t3 v2 in CES)
				{
					if ((get<1>(v2) <= i <= get<0>(v2)) && get<2>(v2) == get<2>(v) +16)
						Vplus++;
				}
			}
			if ((get<0>(v) -get<1>(v) +1) > (0.5 * max(Vplus, Vminus)))
				it = CES.erase(it);
			else
				it++;
		}
		else
			it++;
	}

	printf("Artificial edge assignment.\n");
	
	//Initialize AE to 0
	for (int i = 0; i < ((nHeight / 16) * (nWidth / 16)); i++)
	{
		AE[i] = 0;
	}

	int nWidthMB = nWidth / 16;
	int divider = 256 * nWidthMB;

	//For each pixel loop. We then just need to figure out in what 16x16 block that pixel resides.
	for (int ii = 0; ii < nHeight; ii++)
	{
		for (int jj = 0; jj < nWidth; jj++)
		{
			printf("Current position that is being processed %d, %d      \r", ii, jj);
			//We just extract pixel macroblock
			
			//This calculates in which macroblock pixel resides 
			//for example matrix that has 9 16x16 blocks 
			//	0 1 2
			//	3 4 5
			//	6 7 8
			//x / (16 * 16) * (nWidth/16) and you get some number from 0 - 2 multiply it by (nWidth/16) and you get the macroblock row 
			//The rest part gets the macroblock y position
			//int y = ((x / ((16 * 16) * (nWidth / 16))) * (nWidth / 16)) + ((x / 16) % (nWidth / 16));
			for each  (t3 v in CES)
			{
				if ((get<1>(v) <= ii <= get<0>(v)) && get<2>(v) == jj)
				{
					int x = ii * nWidth + jj;
					int y = ((x / divider) * nWidthMB) + ((x / 16) % nWidthMB);
					AE[y]++;
				}
			}
		}
	}
	printf("\n");

	//Reinitialize variables
	for (int n = 0; n < nWidth * nHeight; n++)
		CE[n] = 0;
	CES.clear();
	//CES.shrink_to_fit();

	vector<int> tempVectorAE;
	//tempVectorAE.resize(nWidth * nHeight);
	for (int i = 0; i < ((nHeight / 16) * (nWidth / 16)); i++)
	{
		tempVectorAE.push_back(AE[i]);
		//printf("AE[%d] = %d\n", i, AE[i]);
	}
	matrixAE.push_back(tempVectorAE);
	duration = (clock() - start) / (double)CLOCKS_PER_SEC;
	printf("                                              TIME = %f\n", duration);
}

void YUVQuality::CalculateMA()
{
	vector<int> tempVector;
	tempVector.reserve((nWidth / 16) * (nHeight / 16));
	for (int i = 0; i < (nWidth / 16) * (nHeight / 16); i++)
	{
		MA[i] = 0;
		tempVector.push_back(MA[i]);
	}
	matrixMA.push_back(tempVector);
}

void YUVQuality::CalculateMF()
{
	vector<bool> tempVector;
	tempVector.reserve((nWidth / 16) * (nHeight / 16));
	for (int i = 0; i < (nWidth / 16) * (nHeight / 16); i++)
	{
		MF[i] = true;
		tempVector.push_back(MF[i]);
	}
	matrixMF.push_back(tempVector);
}

float YUVQuality::CalculateXs()
{
	float sum = 0.0;
	float innerSum = 0.0;
	for (int t = 0; t <= framesProcessed; t++)
	{
		//Because this is summation over MB, every parameter set has the same amount of MB so its not important
		//from which array we shall take how many MB there are
		for (int r = 0; r < matrixAE[t].size(); r++)
		{
			if (matrixEP[t][r] == true)
				innerSum += (matrixMF[t][r] * pow((matrixMA[t][r] + matrixAE[t][r]), 1.5)) / ((1 + matrixTX[t][r]) * matrixTX[t].size());
		}
		sum += pow(innerSum, 0.7) / framesProcessed;
		innerSum = 0.0;
	}
	return sum;

}

void YUVQuality::Reset()
{
	matrixAE.clear();
	matrixEP.clear();
	matrixTX.clear();
	matrixMA.clear();
	matrixMF.clear();
	framesProcessed = 0;
}

float YUVQuality::OnFrameReceived(char* pYBuf, bool bComputeQuality)
{
	printf("///////////////////////////////////////////////////////\n");
	printf("//Current frame being processed     ===>             %d\n", framesProcessed);
	printf("///////////////////////////////////////////////////////\n");
	if (bComputeQuality == false)
	{
		CalculateAE(pYBuf, YUVQuality::nWidth, YUVQuality::nHeight);
		CalculateTX(pYBuf, YUVQuality::nWidth, YUVQuality::nHeight, EP);
		CalculateMA();
		CalculateMF();
	}
	else
	{
		CalculateAE(pYBuf, YUVQuality::nWidth, YUVQuality::nHeight);
		CalculateTX(pYBuf, YUVQuality::nWidth, YUVQuality::nHeight, EP);
		CalculateMA();
		CalculateMF();
		printf("                                            Xs = %f\n",CalculateXs());
		Reset();
	}

	printf("///////////////////////////////////////////////////////\n");
	printf("//Processed frame                   ===>             %d\n", framesProcessed);
	printf("///////////////////////////////////////////////////////\n\n\n");

	framesProcessed++;

	return 0.0;
}

int YUVQuality::Init(int nWidth, int nHeight)
{
	if (TX != NULL)
		delete[] TX;
	TX = new float[(nHeight / 16) * (nWidth / 16)];
	
	if (ST != NULL)
		delete[] ST;
	ST = new float[(nWidth / 16) * (nHeight / 16)];

	if (EDArray != NULL)
		delete[] EDArray;
	EDArray = new float[(nWidth / 16) * (nHeight / 16)];
	
	if (sum != NULL)
		delete[] sum;
	sum = new float[(nWidth / 16) * (nHeight / 16)];
	
	if (meanArray != NULL)
		delete[] meanArray;
	meanArray = new float[(nWidth / 16) * (nHeight / 16)];

	if (texture != NULL)
		delete[] texture;
	texture = new float[nWidth * nHeight];
	
	if (EP != NULL)
		delete[] EP;
	EP = new bool[(nWidth / 16) * (nHeight / 16)];
	
	if (structure != NULL)
		delete[] structure;
	structure = new float[nHeight * nWidth];
	
	if (CE != NULL)
		delete[] CE;
	CE = new int[nWidth * nHeight];

	if (MA != NULL)
		delete[] MA;
	MA = new int[(nWidth / 16) * (nHeight / 16)];

	if (MF != NULL)
		delete[] MF;
	MF = new bool[(nWidth / 16) * (nHeight / 16)];

	if (AE != NULL)
		delete[] AE;
	AE = new int[(nHeight / 16) * (nWidth / 16)];

	framesProcessed = 0;
	b = 0;

	YUVQuality::nWidth = nWidth;
	YUVQuality::nHeight = nHeight;

	return 0;
}

YUVQuality::YUVQuality(int nWidth, int nHeight)
{
	Init(nWidth, nHeight);
}

YUVQuality::~YUVQuality()
{
}
