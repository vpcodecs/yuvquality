// AE and TX of YUV.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stdio.h>
#include <vector>
#include <tuple>
#include <algorithm>
#include <math.h>
#include "YUVQuality.h"


int _tmain(int argc, _TCHAR* argv[])
{

	//read file
	int nWidth, nHeight, nCount;
	FILE * fpIn;
	//FILE * fpOut;
	
	
#pragma warning (disable : 4996)
	fpIn = fopen("C:\\test.yuv", "rb");
	//fpOut = fopen("C:\\result.yuv", "wb");

	nWidth = 1920;
	nHeight = 1080;
	nCount = 5;

	YUVQuality *y_qInstance = new YUVQuality(nWidth,nHeight);

	unsigned int nBytesRead = 0;
	unsigned int nLumaBytes = nWidth*nHeight;
	unsigned int nChromaBytes = nWidth*nHeight / 4;
	char* pYBuf = new char[nLumaBytes];
	char* pUBuf = new char[nChromaBytes];
	char* pVBuf = new char[nChromaBytes];

	for (int i = 0; i < nCount; i++){

		//Start reading frame
		nBytesRead = fread(pYBuf, 1, nLumaBytes, fpIn);
		if (nBytesRead < nLumaBytes)
			break;

		nBytesRead = fread(pUBuf, 1, nChromaBytes, fpIn);
		if (nBytesRead < nChromaBytes)
			break;

		nBytesRead = fread(pVBuf, 1, nChromaBytes, fpIn);
		if (nBytesRead < nChromaBytes)
			break;
		//end reading frame


		// compute AE and TX on Y component of the frame (pYBuf)

		if (i == 4)
			y_qInstance->OnFrameReceived(pYBuf, true);
		else
			y_qInstance->OnFrameReceived(pYBuf, false);

		//fwrite(pYBuf, 1, nLumaBytes, fpOut);
		//fwrite(pUBuf, 1, nChromaBytes, fpOut);
		//fwrite(pVBuf, 1, nChromaBytes, fpOut);
	}

	//fclose(fpOut);
	fclose(fpIn);
	system("PAUSE");
	return 0;
}

