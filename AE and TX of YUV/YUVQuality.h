#pragma once

#include "stdafx.h"
#include <vector>
#include <tuple>
#include <algorithm>
#include <math.h>
#include <ctime>

using namespace std;

class YUVQuality
{
public:
	
	// called for each YUV frame in the video;
	// accumulates Tx, AE for each frame until bComputeQuality is true
	// bComputeQuality == true ==> Compute and return Xs; reset all accumulators for the next group of pictures
	float OnFrameReceived(char* pYUV, bool bComputeQuality);

	// Called before OnFrameReceived(); return 0 on success;
	// allocate memory etc. for given frame width and height
	int Init(int nWidth, int nHeight);

	YUVQuality(int nWidth, int nHeight);
private:
	//Part of the code for LookUp table for exp() function. 
	double e = 2.718281828; //Euler constant
	static const int MAX_ITERATIONS = 22;
	double LUT[MAX_ITERATIONS];
	void initializeLUT();
	void expand_SGL(float x, int* exponent, int* signum, unsigned int* mantissa);
	float e_exp(float x);


	void CalculateTX(char* pYBuf, int nWidth, int nHeight, bool* EP);
	void CalculateAE(char* pYBuf, int nWidth, int nHeight);

	
	
	~YUVQuality();

	//This funtion calculates ED
	float ED(const char* p, int i, int j, int w, int h);

	void CalculateMA();														//Those are setters basically
	void CalculateMF();
	float CalculateXs();
	void Reset();															//Reset all of the parameters
	typedef tuple<int, int, int> t3;
	vector<t3> CES;

	vector<vector<float> > matrixTX;
	vector<vector<int> > matrixAE;
	
	//I assume that MA and MF are going to be calculated in this 
	//same class so I created matrixes for them too
	vector<vector<bool> > matrixMF;
	vector<vector<int> > matrixMA;

	//This is another matrix that needs to be filled but this one is 
	//filled from external source. There is a comment in the code where 
	//I initialize it all as true.
	vector<vector<bool> > matrixEP;

	
	int* AE;
	int* MA;
	bool* MF;
	int nWidth, nHeight;
	float* TX;
	float* ST;
	float* EDArray;
	float* sum;
	float* meanArray;
	float* texture;
	bool* EP;
	float* structure;
	int framesProcessed;						//The number of pictures, frames processed
	int* CE;
	static int beta[7];
	int b = 0;
};

