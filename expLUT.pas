const MAX_ITERATIONS=22;                      //reduce, if less precision and more
var LUT: array [0..MAX_ITERATIONS] of single;    //computing speed is desired
 
procedure initialize_LUT; //create LUT
var index:integer;
begin               //values 0 and MAX_ITERATIONS included in the for loop!
  for index:=0 to MAX_ITERATIONS do LUT[index]:=power(2,power(2,(-index)));
end;
 
procedure expand_SGL(x:single;var exponent:integer; var signum:integer;
                      var mantissa:Cardinal);  //use var type to pass data
var tmp:Cardinal;                     //temporary variable
    pIEEE_754_raw: ^Cardinal;         //pointer to unsigned 32-bit variable
begin
  pIEEE_754_raw:=@x;                  //get address of x
  if (pIEEE_754_raw^ and $80000000)=0 then signum:=0 else signum:=1; //0 :+  1:-
  tmp:=pIEEE_754_raw^ and $7FFFFFFF;  //clear sign bit
  exponent:=(tmp shr 23)-127;         //unbiased exponent
  mantissa:=$80000000 or (tmp shl 8); //set hidden bit and shift rest
end;
 
function two_exp(x:single):single;
var exponent,signum:integer;
    mantissa,test:Cardinal;
    offset,iterations,index,counter:integer;
    temp:single;
begin
  expand_SGL(x,exponent,signum,mantissa);  //extract data
  //special cases
  if exponent<-MAX_ITERATIONS then         //values below precision
  begin
     result:=1                             //case 2^0.0000... = 1
  end else
  if exponent=-MAX_ITERATIONS then        //no multiplications are needed
     begin                                //since the last item is chosen only
        if signum=0 then result:=LUT[MAX_ITERATIONS] else
                         result:=1/LUT[MAX_ITERATIONS]
     end else
     if exponent>7 then         //out of range
        begin
           if signum=0 then result:=1E99 else      //+infinity
                            result:=+0             //+0
        end else
        begin
          iterations:=MAX_ITERATIONS;          //initialize iterations
          //normal cases
          if exponent<0 then
          begin
            offset:=abs(exponent);             //get LUT offset
            iterations:=iterations+exponent;   //less iterations are required //+= in C++
            index:=offset;                     //start LUT at offset
            temp:=LUT[index]                   //initialize temporary variable
          end else
          begin   //exponent>=0
            index:=0;                          //initialize index
            temp:=1                            //initialize temp for products
          end;
 
          for counter:=1 to iterations do      //now iterate
          begin
            index:=index+1;                    //increment index //+= operation in C++
            mantissa:=mantissa shl 1;          //left shift mantissa
            test:=mantissa and $80000000;      //get most significant bit
                                   //ß in Assembly language could use faster bit-test
            if test<>0 then                    //check msb
            begin
              temp:=temp*LUT[index]; //ß could choose normalized multiplying here //*= in C++
            end;
          end;
          //now do the final multiplying, if exponent positive
          if exponent>=0 then result:=2*temp else result:=temp; //ß could fast multiply by 2
                                                           //simply increase result’s
     //binary exponent
 
          if signum=1 then result:=1/result;   //negative value, so inverse
 
          if exponent>0 then
            for counter:=1 to exponent do      //now compute squares
               result:=result*result;          //*= in C++        
        end;
end;
 